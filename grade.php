<?php
# Based on https://github.com/csev/pythonauto/blob/master/grade.php
require_once('lti_util/lti_util.php');
session_start();

if (isset($_REQUEST["exercise"]) && preg_match("/^\d+$/", $_REQUEST["exercise"])) {
  $exercise = $_REQUEST["exercise"];
} else {
  echo json_encode(Array("status" => "failure", "detail" => "No exercise in request"));
  return;
}

if (isset($_REQUEST["code"])) {
  $code = $_REQUEST["code"];
  $grade = "1.0";
} else {
  $grade = "0.0";
}

$context = new BLTI(false, true, false);
if (!$context->valid) {
    echo json_encode(Array("status" => "failure", "detail" => "No context in session"));
    return;
}

$endpoint = $context->getOutcomeService();
if ($endpoint === false) {
    echo json_encode(Array("status" => "failure", "detail" => "No grade service available"));
    return;
}

$sourcedid = $context->getOutcomeSourceDID();
# Hack to work with UNE's LMS
$sourcedid = str_replace("\\\"", "\"", $sourcedid);
if ($sourcedid === false) {
    echo json_encode(Array("status" => "failure", "detail" => "No grade entry available"));
    return;
}

if (isset($_SESSION['oauth_consumer_key']) && isset($_SESSION['oauth_consumer_secret'])
    && $_SESSION['oauth_consumer_key'] !== '' && $_SESSION['oauth_consumer_secret'] !== '') {
    $oauth_consumer_key = $_SESSION['oauth_consumer_key'];
    $oauth_consumer_secret = $_SESSION['oauth_consumer_secret'];
} else {
    echo json_encode(Array("status" => "failure", "detail" => "No key/secret in session"));
    return;
}

function execAvailable() {
  # Function based on http://stackoverflow.com/questions/3938120/check-if-exec-is-disabled#answer-12980534
  $exec_available = true;
  if (ini_get('safe_mode')) {
    $exec_available = false;
  } else {
    $d = ini_get('disable_functions');
    $s = ini_get('suhosin.executor.func.blacklist');
    if ("$d$s") {
      $array = preg_split('/,\s*/', "$d,$s");
      if (in_array('exec', $array)) {
        $exec_available = false;
      }
    }
  }
    return $exec_available;
}

# Ensure code passes tests on server
if (isset($code) && execAvailable() && is_executable("./test_code.sh")) {
    $file = tempnam(sys_get_temp_dir(), "automark_");
    $handle = fopen($file, "w");
    fwrite($handle, $code);
    fclose($handle);
    $output = Array();
    $status = 1;
    exec("./test_code.sh " . $exercise . " " . $file, $output, $status);
    unlink($file);
    if ($status != 0) {
        echo json_encode(Array("status" => "failure", "detail" => "Code does not pass tests"));
        return;
    }
}

$method="POST";
$content_type = "application/xml";
$sourcedid = htmlspecialchars($sourcedid);

$operation = 'replaceResultRequest';
$postBody = str_replace(
	array('SOURCEDID', 'GRADE', 'OPERATION','MESSAGE'),
	array($sourcedid, $grade, $operation, uniqid()),
	getPOXGradeRequest());

try {
    $response = sendOAuthBodyPOST($method, $endpoint, $oauth_consumer_key, $oauth_consumer_secret, $content_type, $postBody);
    $retVal = parseResponse($response);
} catch(Exception $e) {
    echo json_encode(Array("status" => "failure", "detail" => $e->getMessage(), "response" => $response));
    return;
}

if ($retVal["imsx_codeMajor"] == "success") {
  $retVal["status"] = "success";
} else {
  $retVal["status"] = "failure";
  $retVal["detail"] = "Unknown error occurred";
}

echo json_encode($retVal);
?>
