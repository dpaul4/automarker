from exercise import *
from exercises import *
import sys


def get_exercise(id = 0):
    """Loads the exercise to be completed"""
    try:
        id = int(id)
    except ValueError:
        id = 0
    if id < 0 or id >= len(exercises):
        id = 0
    
    return exercises[id]


original_input = input


def output_input(prompt = ""):
    """A function to output the value that is input before it is returned."""
    value = original_input(prompt)
    print(value)
    return value


if __name__ == "__main__":
    exercise_id = sys.argv[-2]
    exercise = get_exercise(exercise_id)
    file = sys.argv[-1]
    fin = open(file)
    code = fin.read()
    if not exercise.run(code, output_input):
        for test in exercise.tests:
            if test.expected_output != test.actual_output:
                print("Expected:", test.expected_output)
                print("Actual:", test.actual_output)
        sys.exit(1)  #  Failed
