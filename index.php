<?php
session_start();
require_once("lti_util/lti_util.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Python Automarker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="css/automarker.css" rel="stylesheet" media="screen" />
    <!--[if lt IE 9]>
      <script src="node_modules/html5shiv/dist/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="node_modules/brython/brython.min.js"></script>
    <script type="text/javascript" src="node_modules/brython/brython_stdlib.js"></script>
    <script type="text/javascript" src="node_modules/monaco-editor/min/vs/loader.js"></script>
    <script type="text/javascript">
      exercise_id = <?php echo isset($_REQUEST["exercise_id"]) ? $_REQUEST["exercise_id"] : 0; ?>;
      editor = document.getElementById('editor');
      jQuery(document).ready(function() {
        require.config({ paths: { vs: 'node_modules/monaco-editor/min/vs' } });
        require(['vs/editor/editor.main'], function () {
          editor = monaco.editor.create(document.getElementById('editor'), {value: "", language: 'python', minimap: {enabled: false}});
          diffEditor = monaco.editor.createDiffEditor(document.getElementById('diffEditor'), {ignoreTrimWhitespace: false});
          brython();
        });
      });
    </script>
  </head>

  <body class="container">

    <div class="accordian col-md-12" id="accordianMain">
      <!-- Instructions -->
      <div class="accordian-item">
        <h2 class="accordian-header" id="headingOne">
          <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <h1 id="exercise">Loading...</h1>
          </button>
        </h2>
        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionMain">
          <div class="accordion-body">
            <span id="instructions">Loading...</span>
          </div>
        </div>
      </div>

       <!-- Code -->
       <div class="accordion-item">
         <h2 class="accordion-header" id="headingTwo">
           <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
             Code
           </button>
         </h2>
         <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingTwo" data-bs-parent="#accordionMain">
           <div class="accordion-body">
             <div id="editor" style="height: 400px;"></div>
             <div class="form-group text-center">
               <button id="run" class="btn btn-secondary">Run</button>
               <button id="test-all" type="button" class="btn btn-primary">Run Tests</button>
               <button id="grade" type="button" class="btn btn-success disabled">Grade</button>
               <span id="nograde" class="visually-hidden">Connect through a LMS to submit grade information</span>
             </div>
             <div><span id="warning"></span></div>
             <div class="visually-hidden" id="output-row">
                 <h3>Output</h3>
                 <div class="form-group">
                   <textarea id="output" readonly class="form-control"></textarea>
                 </div>
           </div>
         </div>
       </div>

       <!-- Tests -->
       <div class="accordion-item">
         <h2 class="accordion-header" id="headingThree">
           <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controlls="collapseThree">
             Tests
           </button>
         </h2>
         <div id="collapseThree" class="accordion-collapse collapse show" aria-labelledby="headingThree" data-bs-parent="#accordionMain">
           <div class="accordion-body">
             <div id="tests" class="list-group"></div>

             <div class="visually-hidden" id="test-row">
                 <h3>Comparison of Expected and Actual Output</h3>
                 <div class="form-group">
                   <div id="diffEditor" style="height:400px; width: 800px;" class="form-control"></div>
                 </div>

                 <h3>Expected Output</h3>
                 <div class="form-group">
                   <textarea id="expected" readonly class="form-control"></textarea>
                 </div>

                <h3>Actual Output</h3>
                <div class="form-group">
                  <textarea id="actual" readonly class="form-control"></textarea>
                </div>

                <h3>Test Input</h3>
                <div class="form-group">
                  <textarea id="input" readonly class="form-control"></textarea>
                </div>

              </div>
            </div>
           </div>
         </div>
       </div>
    </div>

    <script type="text/javascript">
      submission_url = '';
      redirect_url = '';
<?php
  if (!is_lti_request()) {
    echo "jQuery(document).ready(function() {\n";
    echo "    jQuery('#grade').addClass('visually-hidden');\n";
    echo "    jQuery('#nograde').removeClass('visually-hidden');\n";
    echo "    jQuery('#nograde').show();\n";
    echo "});\n";
  } else {
    $oauth_consumer_key = "";
    $oauth_consumer_secret = "";
    if (isset($_POST['oauth_consumer_key'])) {
        $oauth_consumer_key = $_POST['oauth_consumer_key'];
        # TODO: Look this up, rather than just having the same value for everybody
        $oauth_consumer_secret = "12345";
        $_SESSION['oauth_consumer_key'] = $oauth_consumer_key;
        $_SESSION['oauth_consumer_secret'] = $oauth_consumer_secret;
    }
    $context = new BLTI($oauth_consumer_secret, true, false);
    if (!$context->valid) {
      echo "  jQuery(document).ready(function() {\n";
      echo "    jQuery('#grade').addClass('visually-hidden');\n";
      echo "    jQuery('#nograde').removeClass('visually-hidden');\n";
      echo "    jQuery('#nograde').show();\n";
      echo "  });\n";
    } else {
      echo "    submission_url = '" . $context->addSession("grade.php") . "';\n";
      if (isset($_POST['launch_presentation_return_url'])) {
        echo "    redirect_url = '" . $_POST['launch_presentation_return_url'] . "';\n";
      }
    }
  }
?>
      function submitGrade() {
        if (submission_url != "") {
          data = {
            exercise: exercise_id,
            code: editor.getValue()
          };
          jq.post(submission_url, data , function(data, textStatus){
            if (data["status"] == "success") {
              alert("Grade updated!");
              if (redirect_url != '') {
                window.location = redirect_url;
              }
            } else {
              alert("Error updating grade. Please try again.\nError: " + data["detail"]);
            }
          }, "json");
        } else {
          alert("Connect through a LMS to submit grade information");
        }
      }

      function updateDiff() {
        diffEditor.setModel({
          original: monaco.editor.createModel(document.getElementById("expected").value, "text/plain"),
          modified: monaco.editor.createModel(document.getElementById("actual").value, "text/plain")
        });
      }
    </script>
    <script type="text/python" src="automarker_client.py"></script>
  </body>
</html>
