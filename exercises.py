from exercise import *

exercises = []
exercise = Exercise("Error", "Invalid exercise", "", [], [Check("Invalid", ""), Check("Invalid", "", True)])
exercises.append(exercise)

# Exercise 1
exercise = Exercise("Hello World!")
exercise.instructions = "This first exercise is designed to introduce you to the Python Automarker tool.<br/>This area will give you instructions, specifying the exercise you need to complete.<br/>Below is a text area where you can type your code to solve the problem. It is recommended you write and test the code on your system before copying it here to complete the exercise.<br/>Below the code area are three buttons: <ul><li><em>Run</em>: Allows you to run your code to test it is working correctly. You will be prompted to enter any input, and output will be shown below.</li><li><em>Run tests</em>: Runs all of the tests associated with the given exercise (the tests are listed in the Tests area of the page). Input will be taken from the test, and your code's output must match the test's expected output to pass. You can click on an individual test to see its details. You can also run a single test by double-clicking it.</li><li><em>Grade</em>: Once all tests have passed, you can submit your work using this button to have your grade recorded for the exercise.</li></ul>If there is an error in your code, an error message will display in the output area. If a test has failed, click on the test in the list to see that test's output.<br/>Familiarise yourself with the tool. For this exercise, the code you require is already provided (there is no need for you to understand it yet), so ensure you can correctly submit your grade after running all the tests successfully."
exercise.code = "name = input(\"Enter your name: \")\nprint(\"Hello\", name)"
exercise.tests = [Test("Greet Alice", "Enter your name: Alice\nHello Alice\n", "Alice"), Test("Greet Bob", "Enter your name: Bob\nHello Bob\n", "Bob"), Test("Greet Carol", "Enter your name: Carol\nHello Carol\n", "Carol")]
exercise.checks = [Check("Your code requires a print statement", "print")]
exercises.append(exercise)

# Exercise 2
exercise = Exercise("Age Calculator")
exercise.instructions = "Write a program that asks the user to enter their age (in years). If the user is 100 or older, tell them \"You've already turned 100!\". Otherwise, if they are less than 0, tell them \"Try again after you are born!\". If neither of these cases apply, calculate the number of years before they turn 100 and output the message \"You will be 100 in x years!\" (where x is replaced by the number of years before they turn 100). You must use an if...elif...else statement for this problem."
exercise.code = "age = input(\"Enter your current age in years: \")"
exercise.tests = [Test("Over 100", "Enter your current age in years: 101\nYou've already turned 100!\n", "101"),
  Test("Exactly 100", "Enter your current age in years: 100\nYou've already turned 100!\n", "100"),
  Test("Not yet born", "Enter your current age in years: -1\nTry again after you are born!\n", "-1"),
  Test("18 year old", "Enter your current age in years: 18\nYou will be 100 in 82 years!\n", "18"),
  Test("52 year old", "Enter your current age in years: 52\nYou will be 100 in 48 years!\n", "52")]
exercise.checks = [Check("You need to use an if...elif...else statement for this problem", "if"),
  Check("You need to use an if...elif...else statement for this problem", "elif"),
  Check("You need to use an if...elif...else statement for this problem", "else"),
  Check("You need to convert the input to an integer", "\\sint\\(")]
exercises.append(exercise)

# Exercise 3
exercise = Exercise("Right Justify")
exercise.instructions = "Write a function named <em>right_justify</em> that takes a string named <em>input_string</em> as a parameter. If the input string is longer than 79 characters, the string should be printed as is, Otherwise, the function should print the string with enough leading spaces that the last letter of the string is in column 80 of the display. (Note: Python has an inbuilt function called len that returns the length of a string. You can use this function to help perform the task)."
exercise.code = "def right_justify(input_string):\n    pass  # TODO: Complete this function\n\nvalue = input(\"Enter string to justify: \")\nright_justify(value)"
exercise.tests = [Test("Short string", "Enter string to justify: hello\n                                                                           hello\n", "hello"),
  Test("Long string", "Enter string to justify: This is a longer string that does not actually need to be justified. It is already long enough.\nThis is a longer string that does not actually need to be justified. It is already long enough.\n", "This is a longer string that does not actually need to be justified. It is already long enough."),
  Test("Under limit string", "Enter string to justify: This string is exactly 79 characters long & so does require some justification.\n This string is exactly 79 characters long & so does require some justification.\n", "This string is exactly 79 characters long & so does require some justification."),
  Test("Over limit string", "Enter string to justify: This string is exactly 80 characters long and so does not require justification.\nThis string is exactly 80 characters long and so does not require justification.\n", "This string is exactly 80 characters long and so does not require justification.")]
exercise.checks = [Check("You should use len to check the length of the string", "len\("),
  Check("You need a function named right_justify that takes input_string as a parameter", "def right_justify\\(input_string\\)")]
exercises.append(exercise)

# Exercise 4
exercise = Exercise("Paperweight")
exercise.instructions = "Create a dictionary named <em>weight</em>, initialised with the following values:<ul><li>\"pencil\": 10</li><li>\"pen\": 20</li><li>\"paper\": 4</li><li>\"eraser\": 80</li></ul>Create another dictionary named <em>available</em>, initialised with the following values:<ul><li>\"pen\": 3</li><li>\"pencil\": 5</li><li>\"eraser\": 2</li><li>\"paper\": 10</li></ul>If the <em>weight</em> dictionary gives the weight of an individual item of each type, and the <em>available</em> dictionary specifies the number of each item that is available, write code that determines the total weight of all available items (i.e. what is the overall weight of all the pens, pencils, paper, and erasers?)"
exercise.code = "weight = {}  #TODO: Initialise weight with correct values\navailable = {}  # TODO: Initialise available with correct values\n\noverall_weight = 0\n#  TODO: Loop through all available items to determine their overall weight\nprint(\"Overall weight:\", overall_weight)"
exercise.tests = [Test("Weight calculation", "Overall weight: 310\n")]
exercise.checks = [Check("You should use a for loop to loop over all keys in one of the dictionaries", "for "),
  Check("You should calculate the value", "310", True),
  Check("For this exercise, you need to initialise the dicrionary when it is created", "\[[\"']pencil[\"']\](\\s)*=", True),
  Check("For this exercise, you need to initialise the dicrionary when it is created", "\[[\"']pen[\"']\](\\s)*=", True),
  Check("For this exercise, you need to initialise the dicrionary when it is created", "\[[\"']paper[\"']\](\\s)*=", True),
  Check("For this exercise, you need to initialise the dicrionary when it is created", "\[[\"']eraser[\"']\](\\s)*=", True),
  Check("Weights should be numbers, not strings", "[\"']pencil[\"'](\\s)*:(\\s)*[\"']10", True),
  Check("Weights should be numbers, not strings", "[\"']pen[\"'](\\s)*:(\\s)*[\"']20", True),
  Check("Weights should be numbers, not strings", "[\"']paper[\"'](\\s)*:(\\s)*[\"']4", True),
  Check("Weights should be numbers, not strings", "[\"']eraser[\"'](\\s)*:(\\s)*[\"']80", True),
  Check("The number available should be a number, not a string", "[\"']pen[\"'](\\s)*:(\\s)*[\"']3", True),
  Check("The number available should be a number, not a string", "[\"']pencil[\"'](\\s)*:(\\s)*[\"']5", True),
  Check("The number available should be a number, not a string", "[\"']eraser[\"'](\\s)*:(\\s)*[\"']2", True),
  Check("The number available should be a number, not a string", "[\"']paper[\"'](\\s)*:(\\s)*[\"']10", True),
  Check("You need to specify the correct weights in a dictionary", "[\"']pencil[\"'](\\s)*:(\\s)*10"),
  Check("You need to specify the correct weights in a dictionary", "[\"']pen[\"'](\\s)*:(\\s)*20"),
  Check("You need to specify the correct weights in a dictionary", "[\"']paper[\"'](\\s)*:(\\s)*4"),
  Check("You need to specify the correct weights in a dictionary", "[\"']eraser[\"'](\\s)*:(\\s)*80"),
  Check("You need to specify the correct availability in a dictionary", "[\"']pen[\"'](\\s)*:(\\s)*3"),
  Check("You need to specify the correct availability in a dictionary", "[\"']pencil[\"'](\\s)*:(\\s)*5"),
  Check("You need to specify the correct availability in a dictionary", "[\"']eraser[\"'](\\s)*:(\\s)*2"),
  Check("You need to specify the correct availability in a dictionary", "[\"']paper[\"'](\\s)*:(\\s)*10")]
exercises.append(exercise)

# Exercise 5
exercise = Exercise("Bug Hunting")
exercise.instructions = "Debug the following program:"
exercise.code = "Def factorial(n):\n  \"\"\"Calculate the factorial of the given value and return the result.\n\n    The factorial of n is the product of all positive integers less than or equal to n.\n    This function does not support negative values - if a negative value is given, this function just returns 1.\n\n    Arguments:\n    n -- A positive integer\n  \"\"\"\n  result = 1\n  while n != 0:\n    n = n - 1\n    result = result * n\n  return result\n\n# Calculate factorial for the first four integers\nfor i in range(-1, 5):\n  print('Factorial of', i, 'is', factorial(i)"
exercise.tests = [Test("Expected output", "Factorial of 1 is 1\nFactorial of 2 is 2\nFactorial of 3 is 6\nFactorial of 4 is 24\n")]
exercise.checks = [Check("Ensure function begins with \"def\" (not \"Def\")", "def factorial"),
  Check("Should you be testing for inequality only?", "!=", True),
  Check("We only want the factorial for integers 1, 2, 3, and 4", "range\(-1", True)]
exercises.append(exercise)

# Exercise 6
exercise = Exercise("Palindromes")
exercise.instructions = "<p>A <a href=\"https://en.wikipedia.org/wiki/Palindrome\">palindrome</a> is a sequence of characters which reads the same backward or forward. For example, \"anna\" is a palindrome because reversing the letters gives \"anna\", which is the same as when the letters are not reversed.</p><p>Write a recursive function called <em>is_palindrome</em> that takes a string named <em>word</em> as its parameter and returns True if <em>word</em> has length less than or equal to 1. If the length of word is greater than 1, the function should return False if the first character of <em>word</em> is different to the last character of <em>word</em>. If the length of <em>word</em> is greater than 1, and the first and last characters of the string are identical, the function should return the result of <em>is_palindrome()</em> with the parameter of <em>word</em> with its first and last characters removed (e.g. <em>is_palindrome(\"anna\")</em> should return the result of <em>is_palindrome(\"nn\")</em>).</p>"
exercise.code = "def is_palindrome(word):\n    pass  # TODO: Implement this function\n\npossible_palindrome = input(\"Enter a word/phrase to check: \")\nif is_palindrome(possible_palindrome):\n    print(possible_palindrome, \"is a palindrome\")\nelse:\n    print(possible_palindrome, \"is not a palindrome\")"
exercise.tests = [Test("a", "Enter a word/phrase to check: a\na is a palindrome\n", "a"),
  Test("anna", "Enter a word/phrase to check: anna\nanna is a palindrome\n", "anna"),
  Test("banana", "Enter a word/phrase to check: banana\nbanana is not a palindrome\n", "banana"),
  Test("racecar", "Enter a word/phrase to check: racecar\nracecar is a palindrome\n", "racecar"),
  Test("abca", "Enter a word/phrase to check: abca\nabca is not a palindrome\n", "abca"),
  Test("Anna", "Enter a word/phrase to check: Anna\nAnna is not a palindrome\n", "Anna")]
exercise.checks = [Check("You should use len to check the length of the string", "len\\("),
  Check("You need to return a recursive call to is_palindrome", "return(\\s)+(.)*is_palindrome\\("),
  Check("You need a base case in your recursive function", "return(\\s)+[^i][^s]")]
exercises.append(exercise)

# Exercise 7
exercise = Exercise("Turtle Power!")
exercise.instructions = "Read and work through Chapter 4 of the textbook Think Python 2nd Edition.<br />Note: You are probably better off doing this task outside of the browser (e.g. the python3 enviroment on turing) - you can't rerun your code here without refreshing. However, you can try things here if you like - and don't forget to submit something for grading!"
exercise.code = "import turtle\n\ndef square(turtle):\n    for i in range(4):\n        turtle.fd(100)\n        turtle.lt(90)\n\nbob = turtle.Turtle()\nsquare(bob)\n\n# In the browser, you need to use the following rather than turtle.mainloop()\nturtle._Screen().end()"
exercise.tests = []  # Any tests/checks in here don't work with server-side checking
exercise.checks = []
exercises.append(exercise)

# Exercise 8
exercise = Exercise("My Kingdom For A Horse!")
exercise.instructions = "A farrier was contacted one Sunday and asked to shoe a horse. Because Sunday was his day off, the farrier advised the rider to come back tomorrow, and that it would cost $150. But the rider kept insisting that the horse needed to be shod immediately, until eventually the farrier wore down and made the following offer: he would shoe the horse for free, but the rider needed to buy the nails from him and would be charged $0.01 for the first nail, and twice the price of the previous nail for each nail after that (e.g. the second nail would cost $0.02, the third $0.04, and so on). Given that a horse needs four shoes, and each shoe needs six nails, complete the following program to determine the total cost for the rider. It just goes to show how quickly things can grow with exponential growth!"
exercise.code = "total_cost = 0\ncost_of_nail = 1\nfor horse_foot in range(4):\n    for nail in range(6):\n        # TODO: Add price of current nail to total_cost and determine the cost of the next nail\n        pass\n\nprint(\"The total cost to shoe the horse would be ${0:.2f}\".format(total_cost / 100))"
exercise.tests = [Test("Cost Calculator", "The total cost to shoe the horse would be $167772.15\n")]
exercise.checks = [Check("You should perform the calculation rather than hard-coding the value", "167772", True)]
exercises.append(exercise)

# Exercise 9
exercise = Exercise("Viewing People As Objects")
exercise.instructions = "Create a class called <code>Person</code> with the following attributes:<ul><li>name - A string representing the person's name</li></li>age - An int representing the person's age in years</li></ul>As well as appropriate <code>__init__</code> and <code>__str__</code> methods, include the following methods:<ul><li><code>get_name(self)</code> - returns the name of the person</li><li><code>get_age(self)</code> - returns the age of the person</li><li><code>set_name(self, new_name)</code> - sets the name for the person</li><li><code>set_age(self, new_age)</code> - sets the age for the person</li><li><code>is_older_than(self, other)</code> - returns True if this person is older than the one passed as the other parameter, False otherwise"
exercise.code = "class Person:\n    # TODO: Implement this class properly\n    def __init__(self, name, age):\n        pass\n\n    def __str__(self):\n        pass\n\n    def get_name(self):\n        pass\n\n    def get_age(self):\n        pass\n\n    def set_name(self, new_name):\n        pass\n\n    def set_age(self, new_age):\n        pass\n\n    def is_older_than(self, other):\n        pass\n\nalice = Person(\"Alice\", 18)\nbob = Person(\"Bob\", 19)\nif bob.is_older_than(alice):\n    print(\"{0} is older than {1}\".format(bob.get_name(), alice.get_name()))\nelse:\n    print(\"{0} is older than {1}\".format(alice.get_name(), bob.get_name()))"
exercise.tests = [Test("Alice and Bob", "Bob is older than Alice\n")]
exercise.checks = [Check("You should calculate the output", "Bob is older than Alice", True),
  Check("To edit the name for an instance of the Person class, you need to set self.name", "self\.name(\\s)+="),
  Check("To edit the age for an instance of the Person class, you need to set self.age", "self.age(\\s)+=")]
exercises.append(exercise)

# Exercise 10
exercise = Exercise("Test-Driven Development")
exercise.instructions = "An online store requires a Python function to calculate the price of orders. The function should be called <code>calculate_price</code> and take the following two arguments (in order):<ul><li><code>price</code>: A dictionary mapping the name of each item sold by the store to the price of the item (in dollars)</li><li><code>order</code>: A dictionary mapping the name of each item a customer wishes to buy to the quantity of that item they wish to purchase</li></ul>The function should iterate over each item in the order and add the price to buy the desired quantity of that item to the total cost of the order. If any item in the order does not have a corresponding price, your function should raise a <code>KeyError</code>.<p>Once the cost of each item in the order has been calculated, the function should test to see if the overall cost for all items is greater than $100. If it is, a 10% discount should be applied to the overall cost. Otherwise, if the order is greater than $50, a 5% discount should be applied.</p><p>The overall cost of the order (after any applicable discounts) should then be returned by the function.</p><p>Write a function to meet these requirements.</p>"
exercise.code = "def calculate_price(price, order):\n    # TODO: Implement this\n    pass\n\nprice = {'book': 10.0, 'magazine': 5.5, 'newspaper': 2.0}\n\norder1 = {'book': 10}\norder2 = {'book': 1, 'magazine': 3}\norder3 = {'magazine': 5, 'book': 10}\n\nassert(95 == calculate_price(price, order1))\nassert(26.5 == calculate_price(price, order2))\nassert(114.75 == calculate_price(price, order3))\nprint(\"Done\")"
exercise.tests = [Test("Correct implementation", "Done\n")]
exercise.checks = [Check("Please don't alter the assertions", "assert\\(95 == calculate_price\\(price, order1\\)\\)"),
  Check("Please don't alter the assertions", "assert\\(26.5 == calculate_price\\(price, order2\\)\\)"),
  Check("Please don't alter the assertions", "assert\\(114.75 == calculate_price\\(price, order3\\)\\)"),
  Check("Please don't alter the orders", "order1 = \\{'book': 10\\}"),
  Check("Please don't alter the orders", "order2 = \\{'book': 1, 'magazine': 3\\}"),
  Check("Please don't alter the orders", "order3 = \\{'magazine': 5, 'book': 10\\}"),
  Check("Please don't alter the prices", "price = \\{'book': 10.0, 'magazine': 5.5, 'newspaper': 2.0\\}")]
exercises.append(exercise)
