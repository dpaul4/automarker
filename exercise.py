import re
import sys
import traceback

# Pattern to remove Traceback from before an exec call
error_pattern = ".*?\$exec_\d+.*? "
regex_error = re.compile(error_pattern, re.DOTALL)


def execute(code, stdout = sys.stdout, stderr = sys.stderr, stdin = sys.stdin, new_input = input):
    """Executes the given code, with standard output and standard error going to the given values"""
    old_stdout = sys.stdout
    old_stderr = sys.stderr
    old_stdin = sys.stdin

    sys.stdout = stdout
    sys.stderr = stderr
    sys.stdin = stdin

    try:
        available_vars = {}
        available_vars["input"] = new_input
        exec(code, available_vars)
        return True
    except Exception as exc:
        exc.filename = "automarker.py"
        traceback.print_exception(exc)
        return False
    finally:
        sys.stdout = old_stdout
        sys.stderr = old_stderr
        sys.stdin = old_stdin


class Test:
    """A test to run over the given code"""


    def __init__(self, name, expected_output = "", test_input = ""):
        """Gives the test the give name, expected output, and input that it will provide when run"""
        self.name = name
        self.expected_output = expected_output
        self.actual_output = ""
        self.input = test_input
        self.remaining_input = ""


    def write(self, *args):
        """Writes the output to the test object"""
        for arg in args:
            self.actual_output += arg


    def read(self, *args):
        """Reads the input from the test object"""
        if len(self.remaining_input) <= 0:
            return "\n"
        ret_val = self.remaining_input[0]
        self.remaining_input = self.remaining_input[1:]
        return ret_val


    def readline(self, *args):
        """Reads the input from the test object"""
        remaining = self.remaining_input.partition("\n")
        self.remaining_input = remaining[2]
        return remaining[0]


    def close(self, *args):
        """Allow Test to be stdin"""
        pass


    def run(self, code, new_input = input):
        """Executes this test, returning True if the test passes (False otherwise)"""
        self.remaining_input = self.input
        self.actual_output = ""
        execute(code, self, self, self, new_input)
        return self.actual_output == self.expected_output


class Check:
    """Performs a check on the code"""


    def __init__(self, text, regex, ensure_no_match = False):
        """Sets the text and the pattern to match"""
        self.text = text
        self.regex = re.compile(regex)
        self.ensure_no_match = ensure_no_match


    def check(self, code):
        """Ensures this check succeeds"""
        match = self.regex.search(code)
        if match == None:
            return self.ensure_no_match
        return not self.ensure_no_match


class Exercise:
    """The exercise to be completed"""


    def __init__(self, title, instructions = "", code = "", tests = [], checks = []):
        """Sets the title, instructions, initial code, tests, and checks to be performed to complete this exercise"""
        self.title = title
        self.instructions = instructions
        self.code = code
        self.tests = tests
        self.checks = checks
        self.filename = "Exercise"


    def run_checks(self, code):
        """Ensures all checks pass on the given code, returning True if all checks succeed (False otherwise)"""
        all_succeeded = True
        for check in self.checks:
            all_succeeded = check.check(code) and all_succeeded
        return all_succeeded


    def run_tests(self, code, new_input = input):
        """Runs all tests in this exercise, returning True if all succeed (False otherwise)"""
        all_succeeded = True
        for test in self.tests:
            all_succeeded = test.run(code, new_input) and all_succeeded
        return all_succeeded


    def run(self, code, new_input = input):
        """Runs all checks and, if they succeed, all tests, returning True if all checks and tests succeed (False otherwise)"""
        return self.run_checks(code) and self.run_tests(code, new_input)
