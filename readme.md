# Python 3 AutoMarker

An [LTI](http://www.imsglobal.org/activity/learning-tools-interoperability)-based Python autograder similar to [pythonauto](https://github.com/csev/pythonauto), but using [Brython](http://http://brython.info/) to support Python 3.

This project requires PHP support on the server-side. All dependencies are managed by [npm](https://www.npmjs.com/), so after cloning you should run `npm install`.

Before connecting through an [LMS](https://en.wikipedia.org/wiki/Learning_Management_System), you should edit `index.php` to change the `$oauth_consumer_secret` to something unique for your installation.

Using that consumer secret in your LMS, you can then link to `index.php?exercise_id=X`, where X can be 1, 2, 3, 4, 5, 6, 7, 8, 9, or 10 (or edit `exercises.py` to create different exercises).

Students can then click the links to launch the exercises and, once all tests have been passed, they are able to submit their code for grading. By default, grading is client-side only, though you can enable server-side checking by editing `test_code.sh` - note that you should only run this as an unprivileged user, since it allows testing of arbitrary Python code.

You can also visit the exercises outside of an LMS. Testing will still work, though submission of grades is disabled. A [practical demonstration is available](https://turing.une.edu.au/~cosc110/automarker/index.php?exercise_id=1).
