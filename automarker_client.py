from automarker_server import *
from browser import alert
from browser import document
from browser import html
from browser import window


jq = window.jQuery.noConflict(True)
window.jq = jq;
editor = window.editor


def echo_prompt(prompt = ""):
    """Function to make prompt to input appear properly"""
    print(prompt, end = "")
    value = input(prompt)
    print(value, end="\n")
    return value


def fake_input(prompt = ""):
    """Function that avoids input by directly reading standard input, and also echos the prompt"""
    print(prompt, end = "")
    value = sys.stdin.readline()
    print(value, end = "\n")
    return value


class Output:
    """Writes output to #output"""
    def write(self, *args):
        for arg in args:
            jq("#output").val(jq("#output").val() + arg)


def display_exercise(exercise):
    """Displays the given exercise"""
    window.exercise.html = exercise.title
    window.instructions.html = exercise.instructions
    window.editor.setValue(exercise.code)
    window.tests.clear()
    for test_id in range(len(exercise.tests)):
        test = exercise.tests[test_id]
        id = "test_%d" % test_id
        classname = "list-group-item list-group-item-danger"
        list_element = html.A(Class = classname, id = id)
        list_element <= test.name
        list_element.bind("click", show_test)
        list_element.bind("dblclick", run_test)
        window.tests <= list_element
    jq("#output-row").addClass("visually-hidden")
    jq("#test-row").addClass("visually-hidden")


def update_tests(remove_active = False):
    """Checks if all tests have passed, removing the active class if requested"""
    all_passed = True
    jq("#grade").addClass("disabled")
    for test_id in range(len(exercise.tests)):
        test = exercise.tests[test_id]
        jq("#test_%d" % test_id).addClass("list-group-item-danger")
        jq("#test_%d" % test_id).removeClass("list-group-item-success")
        if remove_active:
            jq("#test_%d" % test_id).removeClass("active")
        if test.expected_output == test.actual_output:
            jq("#test_%d" % test_id).addClass("list-group-item-success")
            jq("#test_%d" % test_id).removeClass("list-group-item-danger")
        else:
            all_passed = False
    if all_passed:
        jq("#grade").removeClass("disabled")


def display_test(id):
    """Displays the test with the given id"""
    update_tests(True)
    jq("#output-row").addClass("visually-hidden")
    jq("#test-row").removeClass("visually-hidden")
    jq("#test_%d" % id).addClass("active")
    test = exercise.tests[id]
    jq("#expected").val(test.expected_output)
    jq("#actual").val(test.actual_output)
    jq("#input").val(test.input)
    window.updateDiff()


def show_test(ev):
    """Displays the test that was selected"""
    id = int(ev.target.id[len("test_"):])
    display_test(id)


def run_test(ev):
    """Runs the test that was selected"""
    id = int(ev.target.id[len("test_"):])
    execute_test(exercise.tests[id])
    display_test(id)


def get_code():
    """Gets the code currently in the editor"""
    return window.editor.getValue()


def display_checks(code):
    """Displays alerts for any check that fails - returns True if all checks pass, False otherwise"""
    for check in exercise.checks:
        if not check.check(code):
            jq("#warning").text(check.text)
            alert(check.text)
            return False
    return True


def execute_test(test):
    """Runs the code to complete a test"""
    code = get_code()
    if not display_checks(code):
        return
    test.run(code, fake_input)
    update_tests()


def execute_run(*args):
    """Runs the code for the user to interact with"""
    jq("#test-row").addClass("visually-hidden")
    jq("#output-row").removeClass("visually-hidden")
    jq("#output").val("")
    code = get_code()
    execute(code, output, output, sys.stdin, echo_prompt)


def execute_tests(*args):
    """Executes each test for the exercise"""
    code = get_code()
    if not display_checks(code):
        return
    exercise.run(code, fake_input)
    display_test(0)


def grade_code():
    if jq("#grade").hasClass("disabled"):
        alert("Run all tests successfully to allow grade upload")
    else:
        window.submitGrade();


def code_change(*args):
    """Handles any time the code changes - test results become invalid"""
    jq("#warning").text("")
    jq("#output-row").addClass("visually-hidden")
    jq("#test-row").addClass("visually-hidden")
    for test in exercise.tests:
        test.actual_output = ""
    update_tests(True)




output = Output()

window.exercise.html = "No Exercise Specified"
window.instructions.html = "No exercise was specified, or the exercise is unavailable. Please contact your instructor."
exercise = get_exercise(window.exercise_id)
display_exercise(exercise)

document["run"].bind("click", execute_run)
document["test-all"].bind("click", execute_tests)
document["grade"].bind("click", grade_code)

editor.onDidChangeModelContent(code_change)
